defmodule C do
  
# keyWordsArr = ['каталог', 'справочник', 'нормализация', 'классификация', 'верификация', 'нормативно', 'НСИ', 'МТР']

# http://zakupki.gov.ru/epz/order/quicksearch/search.html
# https://synapsenet.ru/
# https://www.b2b-center.ru/market/
# https://www.rts-tender.ru/auctionsearch
# http://www.sberbank-ast.ru/purchaseList.aspx
# https://www.fabrikant.ru/trades/procedure/search/

@base_url "https://zakupki.gov.ru"
@link_url "/epz/order/notice/ea44/view/common-info.html?regNumber="
@lines_per_page 10
@max_page_number 2

require Elixlsx

alias Elixlsx.Sheet
alias Elixlsx.Workbook


  def t() do
    workbook = %Workbook{sheets: []}
    filters = ["каталог", "справочник", "нормализация", "классификация", "верификация", "нормативно", "НСИ", "МТР"]
    # Enum.reduce(tender_data, sheet, fn item, sheet_acc -> add_data(sheet_acc, item) end)
    Enum.reduce(filters, workbook, fn filter, workbook_acc -> make_report(filter, workbook_acc) end)
      |> Elixlsx.write_to("Zakupki_gov_ru.xlsx")
    # Enum.map(filters, fn filter -> make_sheet(filter, workbook) end)
  end

  def make_report(filter, workbook) do
    #ToDo Получить кол-во страниц по запросу для pages
    pages = 1..@max_page_number
    items = Enum.reduce(pages, [], fn page, items_acc -> get_items(page, filter) ++ items_acc end)
    #ToDo item формат в строке добавляет одинарную кавычку в результирующий файл "'777" вместо "777"
    items
      |> Enum.map(fn item -> get_fields(item) end)
      |> Enum.filter(fn map -> Map.has_key?(map, :city) end)
      |> Enum.with_index(2)
      |> make_sheet(filter, workbook)
  end

  @doc "Получает список элементов со страницы поиска"
  def get_items(page_number, filter) do
    result_html = get_html("https://zakupki.gov.ru/epz/order/extendedsearch/results.html?searchString=#{filter}&pageNumber=#{page_number}&recordsPerPage=_#{@lines_per_page}&fz44=on")
    get_page_items(result_html, ".registry-entry__header-mid__number a")
  end

  @doc "Получает данные из полей карточки"
  def get_fields(item) do
    try do
      result_html1 = get_html("#{@base_url}#{@link_url}#{item}")
      title = get_company_title(result_html1, ".sectionMainInfo__body > div:nth-child(2) > span:nth-child(2) > a:nth-child(1)")
      city = get_city(result_html1, ".container:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(3) > span:nth-child(2)")
      fio = get_fio(result_html1, ".container:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(5) > span:nth-child(2)")
      email = get_email(result_html1, ".container:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(6) > span:nth-child(2)")
      phone = get_phone(result_html1, ".container:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(7) > span:nth-child(2)")
      fax = get_fax(result_html1, ".container:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(8) > span:nth-child(2)")
      %{item: item, title: title, city: city, site: "сайт", fio: fio, email: email, phone: phone, fax: fax}
    rescue
      _error -> %{}
    end
  end
  @doc "получает html код страницы"
  def get_html(url) do
    case HTTPoison.get(URI.encode(url)) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> {:ok, body}
      {:ok, _} -> {:error, "Status not 200"}
      {:error, _} -> {:error, "HTTPoison error"}
    end
  end

  @doc "Получает элементы по поисковому запросу"
  def get_page_items({:ok, html}, selector) do
      html
      |> Floki.find(selector)
      |> Floki.attribute("href")
      |> Enum.filter(fn elem -> String.contains?(elem, "/epz/order/notice/ea44/view/common-info.html?regNumber=") end)
      |> Enum.map(fn elem -> String.replace(elem, "/epz/order/notice/ea44/view/common-info.html?regNumber=", "") end)
  end

  @doc "Получает название организации"
  def get_company_title({:ok, html}, selector) do
      [{_, _, [company_title]}] = html
      |> Floki.find(selector)
    company_title  
  end  

  @doc "Получает город организации"
  def get_city({:ok, html}, selector) do
      [{_, _, [city]}] = html
      |> Floki.find(selector)
    city  
  end  

  @doc "Получает ФИО ответственного лица"
  def get_fio({:ok, html}, selector) do
      [{"span", [{"class", "section__info"}], [fio, {"br", [], []}]}] = html
      |> Floki.find(selector)
    String.trim(fio)
  end  
  
  @doc "Получает email ответственного лица"
  def get_email({:ok, html}, selector) do
      [{"span", [{"class", "section__info"}], [email]}] = html
      |> Floki.find(selector)
    String.trim(email)
  end  

  @doc "Получает номер телефона ответственного лица"
  def get_phone({:ok, html}, selector) do
      [{"span", [{"class", "section__info"}], [phone]}] = html
      |> Floki.find(selector)
    String.trim(phone)
  end  

  @doc "Получает номер факса ответственного лица"
  def get_fax({:ok, html}, selector) do
      [{"span", [{"class", "section__info"}], [fax]}] = html
      |> Floki.find(selector)
    String.trim(fax)
  end  

  @doc "Записывает в файл результаты"
  def make_sheet(tender_data, filter, workbook) do
      sheet =
        Sheet.with_name("Zakupki #{filter}")
        |> Sheet.set_cell("A1", "Статус", bold: true, underline: true, color: "#ffffff",  bg_color: "#3399FF")
        |> Sheet.set_cell("B1", "№", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("C1", "Наименование", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("D1", "Город", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("E1", "Сайт", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("F1", "Контакт: должность, ФИО", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("G1", "E-mail", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("H1", "телефон", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("I1", "Факс", bold: true, underline: true, color: "#ffffff",  bg_color: "#00CC00")
        |> Sheet.set_cell("J1", "Комментарии", bold: true, underline: true, color: "#ffffff",  bg_color: "#3399FF")
        |> Sheet.set_cell("K1", "Дата отправки письма", bold: true, underline: true, color: "#ffffff",  bg_color: "#3399FF")
        |> Sheet.set_cell("L1", "Дата дозвона до компании", bold: true, underline: true, color: "#ffffff",  bg_color: "#3399FF")
        |> Sheet.set_cell("M1", "Дата следующего звонка", bold: true, underline: true, color: "#ffffff",  bg_color: "#3399FF")
        |> Sheet.set_cell("N1", "Бухгалтерия", bold: true, underline: true, color: "#ffffff",  bg_color: "#3399FF")
        |> Sheet.set_cell("O1", "время относительно Москвы", bold: true, underline: true, color: "#000000",  bg_color: "#ffffff")
        |> Sheet.set_cell("P1", "Дата повторной отправки", bold: true, underline: true, color: "#ffffff",  bg_color: "#9966FF")
        |> Sheet.set_cell("Q1", "Дата дозвона до компании при повторной отправке", bold: true, underline: true, color: "#ffffff",  bg_color: "#CC99FF")
        |> Sheet.set_cell("R1", "Комментарий повторной отправки", bold: true, underline: true, color: "#ffffff",  bg_color: "#9966FF")
        |> Sheet.set_cell("S1", "Ответственный", bold: true, underline: true, color: "#000000",  bg_color: "#ffffff")

        sheet = Enum.reduce(tender_data, sheet, fn item, sheet_acc -> add_data(sheet_acc, item) end)

        Workbook.append_sheet(workbook, sheet)
  end

  @doc "Формирует строку с результатом по одной процедуре"
  def add_data(sheet, tender_data) do
    {map_tender, index} = tender_data
    sheet
    |> Sheet.set_cell("B#{index}", map_tender.item)
    |> Sheet.set_cell("C#{index}", map_tender.title)
    |> Sheet.set_cell("D#{index}", map_tender.city)
    |> Sheet.set_cell("E#{index}", map_tender.site)
    |> Sheet.set_cell("F#{index}", map_tender.fio)
    |> Sheet.set_cell("G#{index}", map_tender.email)
    |> Sheet.set_cell("H#{index}", map_tender.phone)
    |> Sheet.set_cell("I#{index}", map_tender.fax)
  end

end
