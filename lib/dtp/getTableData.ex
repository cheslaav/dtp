defmodule Dtp.GetTableData do

  @doc "получает кортеж с данными по кол-ву происшествий" #{:ok, [{"td", [], ["2"]}]}
  def accidents(selector) do
    case HTTPoison.get("https://xn--90adear.xn--p1ai/r/37/divisions/3282") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
      urls =
      body
      |> Floki.find(selector)

      {:ok, urls}
        {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "Not found :("
        {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
    end
  end

  @doc "получает дату, за которую приведена сводка происшествий"
  def get_accidents_date() do
    {:ok, [date]} = accidents("table > tbody > tr:nth-child(1) > th")
    list_with_date = elem(date, 2)
    string_with_date = List.last(list_with_date)
    trimmed_srting = String.trim(string_with_date)
    string_length = String.length(trimmed_srting)
    date_as_string = String.slice(trimmed_srting, string_length-10, string_length)  # "23.01.2021"
    Timex.parse(date_as_string, "{0D}.{0M}.{YYYY}")  # {:ok, ~N[2021-01-23 00:00:00]}
  end

  @doc "получает кол-во происшествий"
    # Для каждой категории соответствует своё значение X. Согласно таблице:
    # ДТП	           x = 2
    # Погибли	       x = 3
    # Погибло детей	 x = 4
    # Ранены	       x = 5
    # Ранено детей	 x = 6
  def get_accidents_count(x) do
    {:ok, [{"td", [], [num]}]} = accidents("table > tbody > tr:nth-child(#{x}) > td:nth-child(2)")
    String.to_integer(num)
  end
end
