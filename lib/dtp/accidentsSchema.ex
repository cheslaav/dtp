defmodule Dtp.AccidentsSchema do
  use Ecto.Schema
  import Ecto.Changeset

  schema "accidents" do
    field :date_accidents, :date
    field :total_accidents, :integer
    field :all_died, :integer
    field :children_died, :integer
    field :injured, :integer
    field :injured_children, :integer
  end
end
