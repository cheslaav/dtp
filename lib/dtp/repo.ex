defmodule Dtp.Repo do
  use Ecto.Repo,
    otp_app: :dtp,
    adapter: Ecto.Adapters.Postgres
end
