defmodule Dtp.Repo.Migrations.CreateAccidentsTable do
  use Ecto.Migration

  def change do
    create table(:accidents) do
      add :date_accidents, :date
      add :total_accidents, :integer
      add :all_died, :integer
      add :children_died, :integer
      add :injured, :integer
      add :injured_children, :integer
    end
  end
end
