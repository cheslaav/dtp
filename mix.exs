defmodule Dtp.MixProject do
  use Mix.Project

  def project do
    [
      app: :dtp,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Dtp.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
  [
    {:ecto_sql, "~> 3.0"},
    {:postgrex, ">= 0.0.0"},
    {:httpoison, "~> 1.7"},
    {:floki, "~> 0.29.0"},
    {:timex, "~> 3.6"},
    {:elixlsx, "~> 0.4.2"}
  ]
  end
end
